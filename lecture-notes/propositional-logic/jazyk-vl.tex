\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{tikz-qtree} % for trees
\usepackage{color} % for truth tables
\usepackage{bussproofs}

\usepackage[czech]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{mathpazo} % add possibly `sc` and `osf` options
\usepackage{eulervm}

\usepackage{framed}

\theoremstyle{definition}
\newtheorem*{definice}{Definice}
\newtheorem*{poznamka}{Poznámka}
\newtheorem*{priklad}{Příklad}
\newtheorem*{reseni}{Řešení}
\newtheorem*{tvrzeni}{Tvrzení}
\newtheorem*{znaceni}{Značení}
\newtheorem*{kzamysleni}{K zamyšlení}

\newcommand{\true}{{\mathbf{tt}}}
\newcommand{\false}{{\mathbf{ff}}}
\renewcommand{\phi}{{\varphi}}
\newcommand{\impl}{\Rightarrow}
\newcommand{\eqv}{\Leftrightarrow}
\newcommand{\ch}[1]{{\mathtt{#1}}}
\newcommand{\word}[1]{{\mathtt{#1}}}
\newcommand{\ew}{\varepsilon}
\newcommand{\cc}{{\mathbin{;}}}

\newcommand{\vDashv}{%
	\mathrel{%
		\text{%
			\ooalign{$\vDash$\cr\reflectbox{$\vDash$}\cr}%
		}%
	}%
}

\newcommand{\semeq}{\mathbin{\vDashv}}
\def\N{{\mathbb{N}}}
\def\Lang{{\mathcal{L}}}
\def\ASCII{{\mathsf{ASCII}}}

\title{Jazyk výrokové logiky}
\author{Matěj Dostál}
\date{}

\usepackage{hyperref}
\begin{document}
	\maketitle

	V tomto textu zavedeme formální jazyk --- jazyk výrokové logiky.

	\section{Znaky a řetězce}

	\begin{poznamka}
		Co je písmeno? Co je znak? Definovat tyto pojmy přesně není snadné. Pokud ale chceme mluvit o jazyku, a to především o jeho psané formě, těžko se bez znaků obejdeme. Velmi neformálně se domluvme na tom, co budeme znakem chápat. Když nazveme např.\ \( \ch{a} \) znakem, myslíme tím, že je to syntaktický objekt, který dále nelze dělit na menší části. Narozdíl od slov, která se \emph{skládají} ze znaků, znaky se už z ničeho neskládají. Slovo \( \word{ahoj} \) se skládá ze znaků \(\ch{a}\), \(\ch{h}\), \(\ch{o}\) a \(\ch{j}\), zatímco znak \( \ch{a} \) se (pro naše potřeby) neskládá z jednodušších částí, a tudíž ho nelze rozložit.

		Ze znaků však lze složit i (v češtině) nesmyslné řetězce. Jako příklad uveďme \( \word{aaabbaaaqqyi} \). Jako uživatelé českého jazyka jsme velmi dobří v rozpoznávání toho, zda je nějaký řetězec českým slovem či nikoli. Zatímco v přirozeném jazyce však mohou být řetězce, u kterých nepanuje shoda (je řetězec \( \word{vyyeetnul} \) českým slovem či nikoli?), u formálního jazyka musíme být schopni pro jakýkoli řetězec jednoznačně rozhodnout, zda do jazyka patří či nikoli.
	\end{poznamka}

	\begin{definice}
		Nechť \( A \) je množina znaků. Množině \( A \) říkejme \emph{abeceda}. Pak jako \( A^* \) značíme množinu všech konečných řetězců (posloupností) znaků z \( A \).
	\end{definice}

	\begin{znaceni}
		Vezměme například množinu \( A = \{ \ch{a}, \ch{b}, \ch{c} \} \). Ačkoli bývá v matematice obvyklé značit např.\ posloupnost pěti znaků \(\ch{a}\) jako \( ( \ch{a}, \ch{a}, \ch{a}, \ch{a}, \ch{a} ) \), dohodněme se nyní, že takovouto posloupnost budeme značit \emph{zřetězením} znaků \( \word{aaaaa} \). Toto značení jistě znáte z programování: prvky množiny \( A \) jsou \uv{typu \( \mathsf{char} \)}, řetězce znaků (např.\ \( \word{aaaaa} \)) jsou \uv{typu \( \mathsf{string} \)}.

		Rozdíl mezi posloupností a řetězcem je tedy čistě ve značení.
	\end{znaceni}

	\begin{priklad}
		Pro množinu \( A = \{ \ch{a}, \ch{b}, \ch{c} \} \) uveďme několik příkladů slov nad \( A \):
		\begin{itemize}
			\item Řetězec \( \word{aabcca} \) je prvkem \( A^* \), řetězcem nad abecedou \( A \).
			\item Řetězec \( \word{bbb} \) je prvkem \( A^* \).
			\item Řetězec \( \ew \), takzvaný \emph{prázdný řetězec}, je prvkem \( A^* \). Tento zvláštní řetězec je také posloupností znaků z abecedy \( A \). Je to posloupnost délky 0, kterou bychom mohli značit takto: \( () \).

			Předejděme zmatení: samotný znak \( \ew \) \emph{není} znakem abecedy \(A\). Tento znak je zástupný, pomáhá nám mluvit o prázdném řetězci a odkazovat se na něj. Napsat prázdný řetězec je totiž obtížné.
			\item Řetězec \( \word{abcd} \) není prvkem \( A^* \), neboť \( \ch{d} \) není znakem z množiny \( A \).
			\item Nekonečná posloupnost \( (\ch{a}, \ch{a}, \ch{a}, \ch{a}, \ch{a}, \ch{a}, \dots ) \) není prvkem \( A^* \). Tento zprava neukončený \uv{řetězec} za řetězec vůbec nepovažujeme.
		\end{itemize}
	\end{priklad}

	\begin{poznamka}
		Řetězce lze skládat. Vezměme například abecedu \( A = \{ \ch{a}, \ch{b}, \ch{c}, \ch{d} \} \) a řetězce \( w_1 = \word{abcd} \) a \( w_2 = \word{ccb} \). Když je zapíšeme za sebe, vznikne řetězec
		\[
		w_1 \cc w_2 = \word{abcd} \cc \word{ccb} = \word{abcdccb}.
		\]
		Jelikož můžeme skládat libovolné dva řetězce, získáváme tak binární operaci
		\[
		\cc: A^* \times A^* \to A^*,
		\]
		která je asociativní, tj.\ pro jakékoli tři řetězce \( w_1, w_2, w_3 \) platí
		\[
		(w_1 \cc w_2) \cc w_3 = w_1 \cc (w_2 \cc w_3),
		\]
		a prázdný řetězec \( \ew \) je vzhledem k této operaci neutrálním prvkem, neboli platí
		\[
		\ew \cc w = w = w \cc \ew
		\]
		pro jakýkoli řetězec \( w \).
	\end{poznamka}

	\begin{znaceni}
		Je častým (a příjemným) zvykem vynechávat symbol pro operaci zřetězení (středník) při řetězení slov. Proto když máme dvě slova $w_1 = \word{abab}$ a $w_2 = \word{bbba}$, často místo zápisu
		\[
		w_1 \cc w_2 = \word{abab} \cc \word{bbba} = \word{ababbbba}
		\]
		píšeme raději
		\[
		w_1 w_2 = \word{ababbbba}.
		\]
	\end{znaceni}

	\begin{poznamka}
		Zápis
		\[
		\cc: A^* \times A^* \to A^*,
		\]
		je často pro studentky a studenty šokující změtí znaků bez zjevného významu. Připomeňme, že máme-li dvě množiny \(X,Y\), je zápis
		\[
		f: X \to Y
		\]
		značením \emph{zobrazení \(f\) z množiny \(X\) do množiny \(Y\)}.
		\begin{itemize}
			\item Například zobrazení
			\begin{align*}
			d: \N & \to \N \\
			n & \mapsto n  + 2
			\end{align*}
			je zobrazení \uv{přičítání dvojky}: každému přirozenému číslu přiřazuje číslo o 2 větší.

			\item I operaci sčítání dvou přirozených čísel můžeme chápat jako zobrazení. Je to zobrazení
			\begin{align*}
			{+}: \N \times \N & \to \N \\
			(m,n) & \mapsto m + n.
			\end{align*}
			Porovnejte značení s předchozím příkladem. Znak \(+\) na prvním řádku je zde označením zobrazení. Sčítání je zobrazení, které jako vstup bere \emph{dvojici} přirozených čísel $(m,n)$, a jako výsledek vrací přirozené číslo \( m + n \). Výstupní hodnoty jsou tedy typu \(\N\) (přirozené číslo), vstupní hodnoty jsou však typu \( \N \times \N\) (dvojice přirozených čísel). Znak \( \times \) je zde \emph{kartézský součin množin}, nikoli násobení přirozených čísel.
		\end{itemize}
		 Po výše uvedených příkladech je tedy snad značení
		 \[
		 \cc: A^* \times A^* \to A^*,
		 \]
		 jasnější. Máme zobrazení (operaci) skládání (zřetězení), kterou značíme symbolem \(;\) (což je v programování častý symbol pro řetězení příkazů). Tato operace má vstupní hodnoty typu \( A^* \times A^* \) (dvojice řetězců nad abecedou \(A\)) a výstupní hodnoty typu \( A^*\) (řetězec znaků z \(A\)).
	\end{poznamka}

	\section{Formální jazyky}

	\begin{poznamka}
		V předchozí části jsme si připravili půdu pro definici formálního jazyka. Náš postup prozatím byl zvolit vhodnou abecedu a všimnout si, že znaky dané abecedy můžeme skládat do řetězců. Popsat formální jazyk \emph{velmi zhruba} znamená popsat jeho gramatiku: které řetězce jsou \uv{gramaticky správně} a tvoří slova daného jazyka? Které řetězce jsou naopak nesmyslnou změtí znaků?
	\end{poznamka}

	\begin{definice}
		Formální jazyk \(\Lang\) nad abecedou \(A\) je podmnožina množiny všech řetězců \( A^* \), tedy \( \Lang \subseteq A^* \).
	\end{definice}

	\begin{poznamka}
		Definice formálního jazyka vypadá podivně. Vezměme abecedu \( Abc = \{ \ch{a}, \ch{b}, \ch{c}, \ch{d}, \dots, \ch{w}, \ch{x}, \ch{y}, \ch{z} \} \). Jakákoli podmnožina množiny $Abc^*$ je podle definice formálním jazykem.
		\begin{itemize}
			\item Například množina \( \Lang_1 = \{ \word{zuzu} \} \subseteq Abc^*\) je formálním jazykem. Obsahuje jediné slovo \(\word{zuzu}\).
			\item Prázdná množina \( \emptyset \subseteq Abc^* \) je formálním jazykem. Tento jazyk neobsahuje žádná slova!
			\item  Sama množina \( Abc^* \) je formálním jazykem. Každý řetězec znaků abecedy \(Abc\) je slovem tohoto jazyka.
		\end{itemize}
		K čemu takovéto jazyky jsou? Poznamenejme, že ačkoli mohou tyto příklady vypadat nesmyslně, v teorii formálních jazyků mají důležitou úlohu. My se ale nebudeme obecnou definicí formálního jazyka trápit. Příklady formálních jazyků, se kterými budeme pracovat, mají bohatou strukturu a jejich použitelnost je očividná.
	\end{poznamka}

	\begin{priklad}
		Ať \( \ASCII \) je abeceda sestávající ze znaků znakové sady ASCII.
		\begin{itemize}
			\item Množina \( \{ w\word{.exe} \mid w \in \ASCII^*,\; w \text{ má délku maximálně } 8 \} \) je formální jazyk, jehož slova jsou řetězce končící znaky \( \word{.exe} \) délky maximálně 12. Jsou to tedy řetězce potenciálních názvů souborů spustitelných v operačním systému DOS.
			\item Množina všech řetězců z \( \ASCII^* \), které jsou kompilovatelnými zdrojovými kódy programů napsaných v jazyce C99, je formální jazyk.
		\end{itemize}
	\end{priklad}

	\begin{poznamka}
		Jazyk výrokové logiky, který brzy zavedeme, má jednoduchou strukturu. Slova tohoto jazyka lze \emph{generovat} několika jednoduchými \emph{generativními pravidly}. Tvorbu jazyka pomocí generativních pravidel nejdříve předvedeme na jednoduchém příkladu jazyka zcela nepraktického, a teprve v další části textu se budeme věnovat přímo jazyku výrokové logiky.
	\end{poznamka}

	\begin{priklad}

		Uvažujme například abecedu \( A = \{ \ch{a}, \ch{b} \} \). Definujme množinu \( Z \subseteq A^* \) následujícími pravidly:
		\begin{itemize}
			\item Slovo \( \word{a} \) do množiny \( Z \) patří. (ax1)
			\item Kdykoli \(w \in Z\), pak i \( \word{a} \cc w \cc \word{a} \in Z\). (rule1)
			\item Kdykoli \(w \in Z\), pak i \( \word{b} \cc w \cc \word{b} \in Z\). (rule2)
			\item Slova, která nelze získat konečně mnoha kroky sestávajícími z použití výše zmíněných pravidel, v množině \( Z \) nejsou.
		\end{itemize}
		Množina $Z$ je formálním jazykem.

		Díky výše popsaným pravidlům je snadné odvodit příklady slov, které do jazyka $Z$ patří. Například díky pravidlu ax1 víme, že \( \word{a} \) je slovem jazyka $Z$. Jelikož \( \word{a} \) je slovem jazyka $Z$, je díky pravidlu rule2 slovem i řetězec \( \word{bab} \).

	\end{priklad}

	\leftbar
	\begin{kzamysleni}
		Uměli byste jazyk $Z$ z předchozího příkladu jednoduše v přirozeném jazyce popsat? Uměli byste popsat nějakou vlastnost, kterou určitě musí mít každé slovo jazyka $Z$?

		Občas je těžké zdůvodnit, proč nějaký řetězec \emph{není} slovem daného formálního jazyka, ačkoli intuitivně slovem zjevně být nemůže. Uměli byste popsat postup (algoritmus) detekující řetězce, které nejsou slovy jazyka $Z$?
	\end{kzamysleni}
	\endleftbar

	\begin{reseni}
		Každé slovo jazyka $Z$ musí mít jistě jako \uv{prostřední} symbol znak \( \ch{a} \). Řetězec $w$ je slovem jazyka $Z$, pokud je to \emph{palindrom} s lichým počtem znaků, jehož prostředním znakem je \( \ch{a} \). Následující postup umí rozhodnout, zda je vstupní řetězec slovem jazyka $Z$ či nikoli.
		\begin{enumerate}
			\item Pokud je řetězec $w$ sudé délky, $w$ \emph{není} slovem jazyka $Z$.
			\item Zkontrolujte, zda má řetězec $w$ délku 1.
			\begin{enumerate}
				\item Pokud má délku 1 a $w = \word{a}$, $w$ \emph{je} slovem jazyka $Z$.
				\item Pokud má délku 1 a $w = \word{b}$, $w$ \emph{není} slovem jazyka $Z$.
				\item Pokud nemá délku 1, pokračujte do bodu 3.
			\end{enumerate}
			\item Z řetězce $w$ odstraňte první a poslední znak.
			\begin{enumerate}
				\item Pokud odstraněné znaky nebyly totožné, $w$ \emph{není} slovem jazyka $Z$.
				\item Pokud odstraněné znaky byly totožné, označte zkrácený řetězec znovu jako $w$ a pokračujte do bodu 2.
			\end{enumerate}
		\end{enumerate}
		Postup je na první pohled správný, dokázat jeho korektnost by však dalo nějakou práci.
	\end{reseni}

	\begin{priklad}[Pokračování předchozího příkladu]
		Častým zvykem je pro pravidla popisující tvorbu slov používat následující zápis:
		\begin{center}
		\AxiomC{}
		\RightLabel{ax1}
		\UnaryInfC{$\word{a}$: $Z$}
		\DisplayProof
		\quad
		\AxiomC{$w$: $Z$}
		\RightLabel{rule1}
		\UnaryInfC{$\word{a}w\word{a}$: $Z$}
		\DisplayProof
		\quad
		\AxiomC{$w$: $Z$}
		\RightLabel{rule2.}
		\UnaryInfC{$\word{b}w\word{b}$: $Z$}
		\DisplayProof
		\end{center}
		Jak tento zápis číst? Každé pravidlo má tři části: vrchní část, dělicí čáru a spodní část.

		Ve vrchní části jsou zapsány předpoklady, za kterých je pravidlo možné použít. Ve spodní části je zapsáno, co můžeme odvodit, jsou-li splněny předpoklady nahoře.

		Pravidlo ax1 má vrchní část prázdnou. Nepotřebujeme tedy nic vědět, abychom odvodili, že \( \word{a} \) je slovem jazyka $Z$.

		Pravidlo rule1 má ve vrchní části napsáno \( w: Z \). Předpokladem k použití rule1 je tedy to, že máme nějaký řetězec \( w \), o kterém už víme, že je slovem jazyka $Z$. Když takové \( w \) máme, můžeme odvodit, že i \( \word{a}w\word{a} \) je slovem jazyka $Z$. Pokud už například víme, že řetězec \( \word{bbbabbb} \) je slovem jazyka $Z$, můžeme díky rule1 odvodit, že i \( \word{abbbabbba} \) je slovem jazyka $Z$. Zcela analogicky funguje pravidlo rule2.

		%		\begin{prooftree}
		%		\AxiomC{A}
		%		\AxiomC{B}
		%		\BinaryInfC{D}
		%		\end{prooftree}

		Odvoďme nyní, že slovo \( \word{bababab} \) do množiny \( Z \) patří. To je vidět z následujícího \uv{odvozovacího stromu}:

		\begin{prooftree}
			\AxiomC{}
			\RightLabel{ax1}
			\UnaryInfC{$\word{a}$: $Z$}
			\RightLabel{rule2}
			\UnaryInfC{$\word{bab}$: $Z$}
			\RightLabel{rule1}
			\UnaryInfC{$\word{ababa}$: $Z$}
			\RightLabel{rule2}
			\UnaryInfC{$\word{bababab}$: $Z$}
		\end{prooftree}
	\end{priklad}

	\leftbar
	\begin{kzamysleni}
		Mějme abecedu $A = \{ \ch{a}, \ch{b} \}$. Jazyk $\Lang$ je podmnožinou množiny $A^*$,
		\[
		\Lang = \{ w \in A^* \mid w \text{ obsahuje stejný počet znaků } \ch{a} \text{ a } \ch{b} \}.
		\]
		Zkuste vymyslet a pojmenovat generativní pravidla pro jazyk $\Lang$. Poté pomocí těchto pravidel odvoďte, že $\word{aaabbb}$, $\word{ababab}$, ${\word{aabbab}}$, $\word{baab}$ a prázdný řetězec jsou slovy jazyka $\Lang$.
	\end{kzamysleni}
	\endleftbar

	\section{Jazyk výrokové logiky}

	\begin{poznamka}
		K čemu bylo nesmyslné hraní si s písmenky v předchozí části textu? Zaměřovali jsme se na jazyky, které byly tvořeny \uv{nesmyslnými} slovy právě proto, že tyto uměle zkonstruované jazyky nemáme spojeny s žádným významem. To nám umožnilo upnout veškerou pozornost ke gramatice, stavbě slov v daném jazyce. Aniž bychom věděli, co dané řetězce znamenají (jelikož neznamenají nic), věděli jsme, který řetězec je správně vytvořeným slovem jazyka a který ne.

		Udržme nyní tento přístup, když budeme definovat jazyk výrokové logiky. Přestože jste se již s výrokovou logikou a s formulemi výrokové logiky setkali, zkuste se oprostit od toho, že formule výrokové logiky mají nějaký význam. Nyní studujeme pouze syntax, gramatiku výrokové logiky. Formulím budeme sémantiku (význam) přisuzovat až později.
	\end{poznamka}

	\begin{poznamka}
		Stejně jako předchozí příklady formálních jazyků i jazyk výrokové logiky bude jistou podmnožinou množiny všech řetězců nad abecedou $A$. Abeceda $A$ bude mít \emph{povinnou část $S$} a \emph{volitelnou část $\At$}, tedy $A = S \cup \At$. Povinná část $S$ je množina symbolů
		\[
		S = \{ \ch{(}, \ch{)}, \ch{\top}, \ch{\bot}, \ch{\neg}, \ch{\wedge}, \ch{\vee}, \ch{\impl}, \ch{\eqv} \},
		\]
		volitelná část $\At$ je část, která nám umožní volit \emph{výrazovou sílu} jazyka výrokové logiky.

		Slova jazyka výrokové logiky budeme nazývat \emph{formule}.
	\end{poznamka}

	\begin{definice}[Jazyk výrokové logiky]
	Mějme množinu takzvaných \emph{atomických formulí} \(\At\). Množina všech formulí výrokové logiky nad množinou \(\At\) je definována induktivně:
	\begin{itemize}
		\item Každá atomická formule $a \in \A$ je formule.
		\item \(\top\) a \(\bot\) jsou formule.
		\item Je-li \(\phi\) formule, pak je i \( ( \neg \phi ) \) formule.
		\item Jsou-li \( \phi_1 \) a \( \phi_2 \) formule, pak je i \( ( \phi_1 \wedge \phi_2 ) \) formule.
		\item Jsou-li \( \phi_1 \) a \( \phi_2 \) formule, pak je i \( ( \phi_1 \vee \phi_2 ) \) formule.
		\item Jsou-li \( \phi_1 \) a \( \phi_2 \) formule, pak je i \( ( \phi_1 \impl \phi_2 ) \) formule.
		\item Jsou-li \( \phi_1 \) a \( \phi_2 \) formule, pak je i \( ( \phi_1 \eqv \phi_2 ) \) formule.
	\end{itemize}
	Řetězec znaků je formule pouze tehdy, když o něm tuto skutečnost můžeme odvodit pomocí výše zmíněných pravidel v konečně mnoha krocích.

	Množinu všech formulí výrokové logiky nad množinou $\At$ značíme $\Fm(\At)$. Jako formální jazyk ji nazýváme jazykem výrokové logiky nad množinou atomických formulí $\At$.
	\end{definice}

	\begin{poznamka}
		Reformulujme nyní definici jazyka výrokové logiky za pomoci generativních pravidel pro formule. Mějme dánu množinu atomických formulí $\At$.
		Pravidla pro tvorbu formulí jsou:
		\begin{center}
			\AxiomC{}
			\RightLabel{atom}
			\UnaryInfC{$a$: fml}
			\DisplayProof
			\quad
			\AxiomC{}
			\RightLabel{top}
			\UnaryInfC{$\top$: fml}
			\DisplayProof
			\quad
			\AxiomC{}
			\RightLabel{bot}
			\UnaryInfC{$\bot$: fml}
			\DisplayProof
%			\quad
%			\AxiomC{$w$: $Z$}
%			\RightLabel{rule1}
%			\UnaryInfC{$\word{a}w\word{a}$: $Z$}
%			\DisplayProof
%			\quad
%			\AxiomC{$w$: $Z$}
%			\RightLabel{rule2.}
%			\UnaryInfC{$\word{b}w\word{b}$: $Z$}
%			\DisplayProof
		\end{center}
		kde $a$ je libovolná atomická formule, tj. pravidel atom je tolik, kolik je atomických formulí,
		\begin{center}
			\AxiomC{$\phi$: fml}
			\RightLabel{neg}
			\UnaryInfC{$(\neg \phi)$: fml}
			\DisplayProof
		\end{center}
		je pravidlo tvrdící, že kdykoli je řetězec $\phi$ formule, je i $(\neg \phi)$ formule, a
		\begin{center}
			\AxiomC{$\phi_1$: fml}
			\AxiomC{$\phi_2$: fml}
			\RightLabel{konj}
			\BinaryInfC{$(\phi_1 \wedge \phi_2)$: fml}
			\DisplayProof
			\quad
			\AxiomC{$\phi_1$: fml}
			\AxiomC{$\phi_2$: fml}
			\RightLabel{disj}
			\BinaryInfC{$(\phi_1 \vee \phi_2)$: fml}
			\DisplayProof
		\end{center}
		\begin{center}
			\AxiomC{$\phi_1$: fml}
			\AxiomC{$\phi_2$: fml}
			\RightLabel{impl}
			\BinaryInfC{$(\phi_1 \impl \phi_2)$: fml}
			\DisplayProof
			\quad
			\AxiomC{$\phi_1$: fml}
			\AxiomC{$\phi_2$: fml}
			\RightLabel{ekv}
			\BinaryInfC{$(\phi_1 \eqv \phi_2)$: fml}
			\DisplayProof
		\end{center}
		jsou pravidla tvrdící, že pokud už víme, že $\phi_1$ a $\phi_2$ jsou formule výrokové logiky, pak i $(\phi_1 \circ \phi_2)$ je formule výrokové logiky (kde $\circ$ je jedním ze symbolů $\wedge,\vee,\impl,\eqv$).
	\end{poznamka}

	\begin{priklad}
		Vezměme si množinu atomických formulí $\At = \{ p, q, r\}$. Odvodíme, že řetězec $((p \wedge q) \impl (\neg r))$ je formulí výrokové logiky (je prvkem $\Fm(\At)$).
		\begin{prooftree}
			\AxiomC{}
			\RightLabel{atom}
			\UnaryInfC{$p$: fml}
			\AxiomC{}
			\RightLabel{atom}
			\UnaryInfC{$q$: fml}
			\RightLabel{konj}
			\BinaryInfC{$(p \wedge q)$: fml}
			\AxiomC{}
			\RightLabel{atom}
			\UnaryInfC{$r$: fml}
			\RightLabel{neg}
			\UnaryInfC{$(\neg r)$: fml}
			\RightLabel{impl}
			\BinaryInfC{$((p \wedge q) \impl (\neg r))$: fml}
		\end{prooftree}
	\end{priklad}

	\leftbar
	\begin{kzamysleni}
		Mějme množinu atomických formulí $\At = \{ p, q, r\}$. Odvoďte, že řetězec $((\neg(p \impl (\neg q))) \vee (\neg (\neg r)))$ je formulí výrokové logiky podobně jako v předchozím příkladu.
	\end{kzamysleni}
	\endleftbar

	\begin{reseni}
		Níže zapsaným stromem odvodíme, že $((\neg(p \impl (\neg q))) \vee (\neg (\neg r)))$ je formulí výrokové logiky.
		\begin{prooftree}
			\AxiomC{}
			\RightLabel{atom}
			\UnaryInfC{$p$: fml}

			\AxiomC{}
			\RightLabel{atom}
			\UnaryInfC{$q$: fml}
			\RightLabel{neg}
			\UnaryInfC{$(\neg q)$: fml}

			\RightLabel{impl}
			\BinaryInfC{$(p \impl (\neg q))$}
			\RightLabel{neg}
			\UnaryInfC{$(\neg (p \impl (\neg q)))$: fml}

			\AxiomC{}
			\RightLabel{atom}
			\UnaryInfC{$r$: fml}
			\RightLabel{neg}
			\UnaryInfC{$(\neg r)$: fml}
			\RightLabel{neg}
			\UnaryInfC{$(\neg (\neg r))$: fml}

			\RightLabel{disj}
			\BinaryInfC{$((\neg(p \impl (\neg q))) \vee (\neg (\neg r)))$}
		\end{prooftree}

	\end{reseni}

	\begin{poznamka}
		Ještě jednou zdůrazněme, že jsme výše zavedli pouze \emph{syntax} výrokové logiky. Jistě jste zvyklí interpretovat znak $\wedge$ jako konjunkci, neboli logickou spojku \uv{a}. My jsme ale zatím žádnému znaku, žádné formuli význam \emph{nepřiřadili}. Pouze jsme popsali, které řetězce znaků jsou \uv{dobře sestavené} a jsou tedy slovy našeho jazyka, formulemi jazyka výrokové logiky.

		Velmi zhruba řečeno: v analogii s programovacími jazyky jsme nyní řekli, co je korektně napsaný zdrojový kód. Nemáme však interpreter, který by programovým konstrukcím (if-then-else klausulím, for cyklům atd.) vdechl život a umožnil použít zdrojový kód k zamýšlenému výpočtu.
	\end{poznamka}

	\begin{znaceni}
		Dovolíme si při zápisu formulí dvěma způsoby \emph{relaxovat} značení:
		\begin{enumerate}
			\item U formulí nebudeme psát vnější závorky. Například formuli
			\[
			((\neg(p \impl (\neg q))) \vee (\neg (\neg r)))
			\]
			můžeme zapisovat jako
			\[
			(\neg(p \impl (\neg q))) \vee (\neg (\neg r)).
			\]
			\item U formulí, ve kterých se vyskytuje negace, nebudeme kolem negace používat závorky. Výše zmíněnou formuli
			\[
			((\neg(p \impl (\neg q))) \vee (\neg (\neg r)))
			\]
			tedy můžeme zapisovat jako
			\[
			\neg(p \impl \neg q) \vee \neg \neg r
			\]
		\end{enumerate}
		Striktně vzato řetězec
		\[
		\neg(p \impl \neg q) \vee \neg \neg r
		\]
		podle naší definice formule formulí \emph{není}. Jelikož je však tento zápis čitelnější a lze z něj jednoznačně určit, jakou formuli by popisoval, kdybychom značení nerelaxovali, nebude naše dohoda o zápisu dělat žádné problémy.

		\emph{Pozor!} V řetězci
		\[
		\neg(p \impl \neg q)
		\]
		jsou závorky důležité. Řetězec
		\[
		\neg p \impl \neg q
		\]
		je v relaxovaném značení \emph{jinou formulí}, a to formulí
		\[
		((\neg p) \impl (\neg q)).
		\]
	\end{znaceni}

	\section{Sémantika výrokové logiky}





%	Abstrakcí našeho příkladu se slovy a operací zřetězování (skládání) získáme algebraickou strukturu, jež se nazývá monoid.
%
%	\begin{definice}
%		Mějme množinu \( M \), na této množině mějme asociativní binární operací \( {\star}: M \times M \to M \), a dále mějme prvek $e \in M$, který je vzhledem k operaci \( {\star} \) neutrální. Trojici \( (M,{\star},e) \) nazýváme \emph{monoid}.
%	\end{definice}
%
%	\begin{priklad}
%		Množina \( A^* \) všech slov nad abecedou \( A \) spolu s operací zřetězení a spolu s prázdným slovem tvoří monoid.
%	\end{priklad}

%	\emph{Strukturální indukcí} lze definovat zajímavé podmnožiny množiny všech slov nad abecedou.
%
%
%
%	\begin{priklad}
%		Vezměme znovu abecedu \( A = \{ \ch{a}, \ch{b} \} \) a definujme množinu \( Y \subseteq A^* \) následujícími pravidly:
%			\begin{itemize}
%			\item Slovo \( \word{a} \) do množiny \( Y \) patří.
%			\item Kdykoli \(w \in Y\), pak i \( \word{a} \cc w \cc \word{a} \in Z\).
%			\item Kdykoli \(w_1 \in Y\) a \(w_2 \in Y\), pak i \( w_1 \cc \word{b} w_2 \in Z\).
%			\item Slova, která nelze získat konečně mnoha kroky sestávajícími z použití výše zmíněných pravidel, v množině \( Z \) nejsou.
%		\end{itemize}
%	\end{priklad}

%	\begin{definice}
%		Mějme množinu znaků \( A \) a množinu \( A^* \) slov nad abecedou \( A \).
%	\end{definice}



%	{\color{red} TODO }
%
%	Následující tvrzení formalisuje myšlenku, že logická spojka ekvivalence \uv{internalisuje} vztah sémantické ekvivalence.
%
%	\begin{tvrzeni}[Vztah sémantické ekvivalence a spojky ekvivalence]
%		Pro jakékoli dvě formule výrokové logiky $\alpha$ a $\beta$ platí:
%		\[
%		\alpha \semeq \beta \qquad \text{ právě tehdy, když } \qquad \alpha \eqv \beta \; \text{ je tautologie.}
%		\]
%	\end{tvrzeni}
%
%	\begin{proof}
%		Sémantická ekvivalence $\alpha \semeq \beta$ znamená, že pro každé pravdivostní ohodnocení $u$ platí $u(\alpha) = u(\beta)$. To je pravda právě tehdy, když pro každé pravdivostní ohodnocení $u$ platí $u(\alpha \eqv \beta) = 1$, což z definice znamená, že $\alpha \eqv \beta$ je tautologie.
%	\end{proof}
%
%	Spojka ekvivalence nám tedy dává způsob, jak zapsat \emph{uvnitř jazyka výrokové logiky} tvrzení, že jsou dvě formule sémanticky ekvivalentní.
%
%	\begin{priklad}
%		Známe-li například sémantickou ekvivalenci $\alpha \wedge \beta \semeq \beta \wedge \alpha$, můžeme s použitím výše odvozeného vztahu sémantické ekvivalence a spojky ekvivalence bez dalšího počítání vyvodit, že formule $(\alpha \wedge \beta) \eqv (\beta \wedge \alpha)$ je tautologie.
%	\end{priklad}
%
%	\begin{poznamka}
%		Zápis $$(a \wedge b \wedge c)$$ nechápeme jako formuli vzniklou aplikací ternární logické spojky na tři atomické formule, ale jako relaxaci zápisu formule $$((a \wedge b) \wedge c).$$
%		Podobně zápis $$(a \vee b \vee c \vee d)$$ nechápeme jako formuli vzniklou aplikací kvaternární logické spojky na čtyři atomické formule, ale jako relaxaci formule $$(((a \vee b) \vee c) \vee d).$$
%		Důsledkem této volby je mimo jiné to, že formule $$\phi = (a \wedge b \wedge c)$$ má hloubku 2, nikoli 1, a její syntaktický strom je
%		\begin{center}
%			\begin{tikzpicture}
%			\Tree [.$\wedge$ [.$\wedge$ $a$ $b$ ] $c$. ]
%			\end{tikzpicture}
%		\end{center}
%	\end{poznamka}
%
%	Zobecněme nyní naši dohodu z předchozí poznámky.
%
%	\begin{znaceni}
%		Máme-li dány formule $\phi_1, \dots, \phi_n$, pro úsporné vyjadřování využíváme syntaktickou zkratku
%		$$\bigwedge_{i=1}^n \phi_i,$$
%		která je
%		\begin{itemize}
%			\item pro $n = 0$ definována jako formule $\mathbf{tt}$,
%			\item pro $n = 1$ definována jako formule $\phi_1$,
%%			\item pro $n = 2$ definována jako $(\phi_1 \wedge \phi_2)$, a
%			\item pro $n > 2$ definována induktivně následovně:
%			\[
%			\bigwedge_{i=1}^n \phi_i = \left( \bigwedge_{i=1}^{n-1} \phi_i \wedge \phi_n \right).
%			\]
%		\end{itemize}
%	\end{znaceni}
%
%	\begin{priklad}
%		Např. pro $n = 2$ se formule $\bigwedge_{i=1}^2 \phi_i$ rovná formuli $(\phi_1 \wedge \phi_2)$.
%
%		Pro $\phi_1 = a$, $\phi_2 = b$ a $\phi_3 = c$ je formule $\bigwedge_{i=1}^3 \phi_i$ rovna formuli $\phi = (a \wedge b) \wedge c$ výše, tj.\ formuli se syntaktickým stromem
%		\begin{center}
%			\begin{tikzpicture}
%			\Tree [.$\wedge$ [.$\wedge$ $a$ $b$ ] $c$. ]
%			\end{tikzpicture}
%		\end{center}
%	\end{priklad}


























%	\section*{Příklady z výrokové logiky}
%	\subsection*{Základy syntaxe a sémantiky}
%	V zadání příkladů chybí explicitně zmíněná množina logických proměnných. Prosím, předpokládejte v těchto cvičeních, že množinou logických proměnných je $A = \{ x,y,z,v \}$.
%
%
%	\begin{reseni}[Řešení příkladu 1.1.1]
%		\begin{enumerate}
%			\item
%			Daný řetězec je formule, její syntaktický strom je
%			\[
%				\begin{tikzpicture}%[sibling distance=72pt]
%				\Tree
%				[.$\neg$
%					[.$\impl$
%						[.$\neg$ $x$ ]
%						[.$\neg$
%							[.$\neg$ $x$ ]
%						]
%					]
%				]
%				%\Tree [.$\wedge$ [.$\wedge$ $a$ $b$ ] $c$. ]
%				\end{tikzpicture}
%			\]
%			a její hloubka je 4.
%
%			\item
%			Daný řetězec je formule, její syntaktický strom je
%			\[
%				\begin{tikzpicture}[sibling distance=72pt]
%				\Tree
%				[.$x$ ]
%				\end{tikzpicture}
%			\]
%			a její hloubka je 0.
%
%			\item
%			Daný řetězec je formule, její syntaktický strom je
%			\[
%			\begin{tikzpicture}%[sibling distance=72pt]
%			\Tree
%			[.$\impl$
%				[.$\vee$
%					[.$\neg$ $x$ ]
%					$y$
%				]
%				$z$
%			]
%			%\Tree [.$\wedge$ [.$\wedge$ $a$ $b$ ] $c$. ]
%			\end{tikzpicture}
%			\]
%			a její hloubka je 3.
%
%			\item
%			Daný řetězec není formule. Chybí závorky, které by určily, zda je v kořeni syntaktického stromu konjunkce či ekvivalence.
%
%			\item
%			Daný řetězec je formule, její syntaktický strom je
%			\[
%			\begin{tikzpicture}
%			\Tree
%			[.$\eqv$
%				[.$\impl$
%					[.$\vee$
%						[.$\neg$
%							[.$\wedge$
%								$x$
%								$y$
%							]
%						]
%						$z$
%					]
%					$u$
%				]
%				$v$
%			]
%			\end{tikzpicture}
%			\]
%			a její hloubka je 5.
%
%			\item
%			Daný řetězec není formule, $\Leftarrow$ není v našem jazyce logická spojka.
%		\end{enumerate}
%	\end{reseni}
%
%	\begin{reseni}[Řešení příkladu 1.1.2]
%		Jedním ze způsobů řešení je tvorba pravdivostní tabulky dané formule, ze které lze vyčíst, v jakých pravdivostních ohodnoceních je formule pravdivá či nepravdivá.
%
%		Pravdivostní tabulky v našem řešení jsou \uv{komprimované}, pod každou spojkou je sloupec pravdivostních hodnot, který odpovídá pravdivosti formule \uv{generované danou spojkou}.
%		\begin{enumerate}
%			\item
%			Daná formule má následující pravdivostní tabulku:
%			\[
%			\begin{tabular}{@{ }c | c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c}
%			$x$ &  & ( & $x$ & $\impl$ & $\neg$ & $x$ & ) & $\wedge$ & ( & $\neg$ & $x$ & $\impl$ & $x$ & ) & \\
%			\hline
%			1 &  &  & 1 & 0 & 0 & 1 &  & \textcolor{red}{0} &  & 0 & 1 & 1 & 1 &  & \\
%			0 &  &  & 0 & 1 & 1 & 0 &  & \textcolor{red}{0} &  & 1 & 0 & 0 & 0 &  & \\
%			\end{tabular}
%			\]
%			Formule není pravdivá v žádném pravdivostním ohodnocení.
%
%			\item
%			Daná formule má následující pravdivostní tabulku:
%			\[
%			\begin{tabular}{@{ }c@{ }@{ }c | c@{ }@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c}
%			$x$ & $y$ &  & $x$ & $\impl$ & ( & $x$ & $\impl$ & $y$ & ) & \\
%			\hline
%			1 & 1 &  & 1 & \textcolor{red}{1} &  & 1 & 1 & 1 &  & \\
%			1 & 0 &  & 1 & \textcolor{red}{0} &  & 1 & 0 & 0 &  & \\
%			0 & 1 &  & 0 & \textcolor{red}{1} &  & 0 & 1 & 1 &  & \\
%			0 & 0 &  & 0 & \textcolor{red}{1} &  & 0 & 1 & 0 &  & \\
%			\end{tabular}
%			\]
%			Je nepravdivá v pravivostním ohodnocení $u$, pro které platí $u(x) = 1$ a $u(y) = 0$.
%
%			\item
%			Daná formule má následující pravdivostní tabulku:
%			\[
%			\begin{tabular}{@{ }c@{ }@{ }c@{ }@{ }c | c@{ }@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{ }c@{ }@{}c@{}@{}c@{}@{ }c}
%				$x$ & $y$ & $z$ &  & $x$ & $\wedge$ & ( & $y$ & $\impl$ & ( & $z$ & $\vee$ & $x$ & ) & ) & \\
%				\hline
%				1 & 1 & 1 &  & 1 & \textcolor{red}{1} &  & 1 & 1 &  & 1 & 1 & 1 &  &  & \\
%				1 & 1 & 0 &  & 1 & \textcolor{red}{1} &  & 1 & 1 &  & 0 & 1 & 1 &  &  & \\
%				1 & 0 & 1 &  & 1 & \textcolor{red}{1} &  & 0 & 1 &  & 1 & 1 & 1 &  &  & \\
%				1 & 0 & 0 &  & 1 & \textcolor{red}{1} &  & 0 & 1 &  & 0 & 1 & 1 &  &  & \\
%				0 & 1 & 1 &  & 0 & \textcolor{red}{0} &  & 1 & 1 &  & 1 & 1 & 0 &  &  & \\
%				0 & 1 & 0 &  & 0 & \textcolor{red}{0} &  & 1 & 0 &  & 0 & 0 & 0 &  &  & \\
%				0 & 0 & 1 &  & 0 & \textcolor{red}{0} &  & 0 & 1 &  & 1 & 1 & 0 &  &  & \\
%				0 & 0 & 0 &  & 0 & \textcolor{red}{0} &  & 0 & 1 &  & 0 & 0 & 0 &  &  & \\
%			\end{tabular}
%			\]
%			Je pravdivá přesně v těch pravdivostních ohodnoceních $u$, pro která platí $u(x) = 1$.
%
%			\item
%			Daná formule má následující pravdivostní tabulku:
%			\[
%			\begin{tabular}{@{ }c | c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{ }c@{ }@{ }c}
%			$x$ &  & ( & $x$ & $\impl$ & $\neg$ & $x$ & ) & $\impl$ & $\neg$ & $x$ & \\
%			\hline
%			1 &  &  & 1 & 0 & 0 & 1 &  & \textcolor{red}{1} & 0 & 1 & \\
%			0 &  &  & 0 & 1 & 1 & 0 &  & \textcolor{red}{1} & 1 & 0 & \\
%			\end{tabular}
%			\]
%			Není nepravdivá v žádném pravdivostním ohodnocení. (Je pravdivá ve všech pravdivostních ohodnoceních.)
%
%			\item
%			Daná formule má následující pravdivostní tabulku:
%			\[
%			\begin{tabular}{@{ }c@{ }@{ }c | c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c}
%			$x$ & $y$ &  & ( & $x$ & $\vee$ & $\neg$ & $y$ & ) & $\impl$ & ( & $\neg$ & $x$ & $\wedge$ & $y$ & ) & \\
%			\hline
%			1 & 1 &  &  & 1 & 1 & 0 & 1 &  & \textcolor{red}{0} &  & 0 & 1 & 0 & 1 &  & \\
%			1 & 0 &  &  & 1 & 1 & 1 & 0 &  & \textcolor{red}{0} &  & 0 & 1 & 0 & 0 &  & \\
%			0 & 1 &  &  & 0 & 0 & 0 & 1 &  & \textcolor{red}{1} &  & 1 & 0 & 1 & 1 &  & \\
%			0 & 0 &  &  & 0 & 1 & 1 & 0 &  & \textcolor{red}{0} &  & 1 & 0 & 0 & 0 &  & \\
%			\end{tabular}
%			\]
%			Je pravdivá v ohodnocení $u$, pro které platí $u(x) = 0$ a $u(y) = 1$.
%
%			\item
%			Daná formule má následující pravdivostní tabulku:
%			\[
%			\begin{tabular}{@{ }c@{ }@{ }c@{ }@{ }c | c@{ }@{}c@{}@{}c@{}@{}c@{}@{ }c@{ }@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{}c@{}@{ }c@{ }@{ }c@{ }@{ }c}
%			$x$ & $y$ & $z$ &  & ( & ( & ( & $x$ & $\vee$ & $y$ & ) & $\wedge$ & $z$ & ) & $\wedge$ & $y$ & ) & $\vee$ & $x$ & \\
%			\hline
%			1 & 1 & 1 &  &  &  &  & 1 & 1 & 1 &  & 1 & 1 &  & 1 & 1 &  & \textcolor{red}{1} & 1 & \\
%			1 & 1 & 0 &  &  &  &  & 1 & 1 & 1 &  & 0 & 0 &  & 0 & 1 &  & \textcolor{red}{1} & 1 & \\
%			1 & 0 & 1 &  &  &  &  & 1 & 1 & 0 &  & 1 & 1 &  & 0 & 0 &  & \textcolor{red}{1} & 1 & \\
%			1 & 0 & 0 &  &  &  &  & 1 & 1 & 0 &  & 0 & 0 &  & 0 & 0 &  & \textcolor{red}{1} & 1 & \\
%			0 & 1 & 1 &  &  &  &  & 0 & 1 & 1 &  & 1 & 1 &  & 1 & 1 &  & \textcolor{red}{1} & 0 & \\
%			0 & 1 & 0 &  &  &  &  & 0 & 1 & 1 &  & 0 & 0 &  & 0 & 1 &  & \textcolor{red}{0} & 0 & \\
%			0 & 0 & 1 &  &  &  &  & 0 & 0 & 0 &  & 0 & 1 &  & 0 & 0 &  & \textcolor{red}{0} & 0 & \\
%			0 & 0 & 0 &  &  &  &  & 0 & 0 & 0 &  & 0 & 0 &  & 0 & 0 &  & \textcolor{red}{0} & 0 & \\
%			\end{tabular}
%			\]
%			Je nepravdivá ve všech pravdivostních ohodnoceních $u$, pro která platí $u(x) = 0$ a současně neplatí $u(y) = u(z) = 1$.
%		\end{enumerate}
%	\end{reseni}
%
%	\begin{reseni}[Řešení příkladů 1.1.3 a 1.1.4]
%		Toto a další cvičení lze znovu snadno řešit tvorbou pravdivostních tabulek. Pro kontrolu lze využít např.\ generátor pravdivostních tabulek na adrese
%		\begin{center}
%			\url{https://mrieppel.net/prog/truthtable.html}
%		\end{center}
%	\end{reseni}
%
%	\begin{reseni}[Řešení příkladu 1.1.5]
%		Víme, že $\alpha$ je tautologie, $\beta$ kontradikce, o $\phi$ víme jen to, že je to formule. Dokážeme následující tři tvrzení:
%		\begin{enumerate}
%			\item Formule
%			\[
%			(\beta \vee \phi) \eqv \phi
%			\]
%			je tautologie.
%
%			Vezměme libovolné pravdivostní ohodnocení $u$. Z předpokladu víme, že platí $u(\beta) = 0$. Pak $u((\beta \vee \phi) \eqv \phi) = 1$ platí přesně tehdy, pokud platí rovnost $u(\beta \vee \phi) = u(\phi)$. Z významu spojky disjunkce víme, že $u(\beta \vee \phi) = 0$ právě tehdy, pokud $u(\beta) = 0$ a $u(\phi) = 0$. Proto $u(\beta \vee \phi) = 0$ právě tehdy, pokud $u(\phi) = 0$. Platí tedy $u(\beta \vee \phi) = u(\phi)$, což dokazuje, že $(\beta \vee \phi) \eqv \phi$ je tautologie.
%
%			\item
%			Formule
%			\[
%			(\alpha \vee \phi)
%			\]
%			je tautologie.
%
%			Kdykoli $u(\alpha) = 1$, pak ze sémantiky disjunkce můžeme odvodit, že $u(\alpha \vee \phi) = 1$. Ale $u(\alpha) = 1$ platí pro libovolné $u$, a $(\alpha \vee \phi)$ je tedy tautologie.
%
%			\item
%			Formule
%			\[
%			(\beta \wedge \phi)
%			\]
%			je kontradikce.
%
%			Aby platilo $u(\beta \wedge \phi) = 1$, muselo by platit $u(\beta) = 1$ a $u(\phi) = 1$. Ale $\beta$ je kontradikce, a tedy platí $u(\beta) = 0$. Pro libovolné pravdivostní ohodnocení $u$ tedy platí, že $u(\beta \wedge \phi) = 0$, formule $\beta \wedge \phi$ je tudíž kontradikce.
%		\end{enumerate}
%	\end{reseni}
%
%
%	\begin{reseni}[Řešení příkladu 1.1.6]
%		Jestliže je formule $\phi \impl \psi$ tautologie, pak jsou tautologiemi i formule $(\phi \wedge \psi) \eqv \phi$ a $(\phi \vee \psi) \eqv \psi$.
%
%		Vyřešíme nejprve případ s formulí $(\phi \wedge \psi) \eqv \phi$. Vezměme libovolné pravdivostní ohodnocení $u$.
%		\begin{itemize}
%			\item Platí-li $u(\phi) = 1$, pak z $u(\phi \impl \psi) = 1$ plyne $u(\psi) = 1$. Proto $u(\phi \wedge \psi) = 1 = u(\phi)$, a tudíž $u((\phi \wedge \psi) \eqv \phi) = 1$.
%
%			\item Platí-li $u(\phi) = 0$, pak $u(\phi \wedge \psi) = 0$, a tudíž $u((\phi \wedge \psi) \eqv \phi) = 1$.
%		\end{itemize}
%		Formule $(\phi \wedge \psi) \eqv \phi$ je tedy tautologie.
%
%		Nyní vyřešme případ s formulí $(\phi \vee \psi) \eqv \psi$. Vezměme libovolné pravdivostní ohodnocení $u$.
%		\begin{itemize}
%			\item Platí-li $u(\phi) = 1$, pak $u(\phi \vee \psi) = 1$ a z $u(\phi \impl \psi) = 1$ plyne $u(\psi) = 1$. Proto $u(\phi \vee \psi) = 1 = u(\psi)$, a tudíž $u((\phi \vee \psi) \eqv \psi) = 1$.
%
%			\item Platí-li $u(\phi) = 0$, pak $u(\phi \vee \psi) = u(\psi)$, a tudíž $u((\phi \vee \psi) \eqv \psi) = 1$.
%		\end{itemize}
%	Formule $(\phi \vee \psi) \eqv \psi$ je tedy tautologie.
%
%	\end{reseni}
\end{document}